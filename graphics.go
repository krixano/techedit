package main

import (
"fmt"
"github.com/nsf/termbox-go"
"strconv"
"sync"
)


var menuTitle = "Apeeling-Tech TechEdit\n v0.4 alpha  -   Menu"
var menuSelectionX int

func drawEditorMenu(swg *sync.WaitGroup) {
	var wg sync.WaitGroup

	wg.Add(4)

	// Title Bar
	go writeWG(&wg, "Apeeling-Tech TechEdit - v0.4 alpha", 0, 0, termbox.ColorBlack, termbox.ColorCyan, false, true, termbox.ColorCyan)
	go writeWG(&wg, CurrentTime, width-5, 0, termbox.ColorBlack, termbox.ColorCyan, false, true, termbox.ColorCyan)

	// Side Margin (left)
	go drawLineWG(&wg, editorX-3, 1, editorX-2, height-1-editorBY /*0xFA*/, termbox.ColorCyan, BGColor, "y")

	// Status Bar (bottom)
	go updateEditorStatusWG(&wg, 0)

	wg.Wait()
	swg.Done()
}

func updateEditorStatus(offset int) {
	if Input != "" {
		status = ""
	} else {
		status = status
	}
	var wg sync.WaitGroup
	wg.Add(2)
	go writeWG(&wg, status, 0+offset, height-1, FGColor, termbox.ColorRed, false, false, BGColor)
	text := fmt.Sprintf(" Current - %d:%d End - %d:%d | Indent - %d TabMode - %s", CurrentX-editorX, CurrentY-(editorY-1), endx-editorX, endy-(editorY-1), indentLevels[CurrentY], tabMode)
	go writeWG(&wg, text, len(status)+offset, height-1, termbox.ColorBlack, termbox.ColorCyan, false, true, termbox.ColorCyan) // TODO
	wg.Wait()
}

func updateEditorStatusWG(swg *sync.WaitGroup, offset int) {
	updateEditorStatus(offset)
	swg.Done()
}

func drawEditorLine(swg *sync.WaitGroup) {
	drawLine(editorLine+editorX-1, editorY, editorLine+editorX, height-2 /*0x1C*/, termbox.ColorCyan, BGColor, "v")
	swg.Done()
}

func drawMenu(swg *sync.WaitGroup) {
	var wg sync.WaitGroup
	wg.Add(len(currentMenu) + 2)
	go writeWG(&wg, menuTitle, (width/2)-(len(menuTitle)/4), (height/2)-10, FGColor, BGColor, true, false, BGColor)
	go writeWG(&wg, CurrentTime, width-5, 0, termbox.ColorBlack, termbox.ColorCyan, false, true, BGColor)
	for i, v := range currentMenu {
		if i != len(currentMenu)-1 {
			go writeWG(&wg, fmt.Sprintf("%s", v), (width/2)-(len(menuTitle)/4)+3, ((height/2)-10)+3+i, FGColor, BGColor, true, false, BGColor)
		} else {
			go writeWG(&wg, fmt.Sprintf("%s", v), (width/2)-(len(menuTitle)/4)+3, ((height/2)-10)+3+i, FGColor, BGColor, true, false, BGColor)
		}
	}
	wg.Wait()
	swg.Done()
}

func updateMenuSelected(swg *sync.WaitGroup) {
	if menuSelectionX == 0 {
		menuSelectionX = (width / 2) - (len(menuTitle) / 4) + 1
	}
	var wg sync.WaitGroup
	wg.Add(len(currentMenu))
	for i, _ := range currentMenu {
		if i == menuSelected {
			go setCellWG(&wg, menuSelectionX, ((height/2)-10)+3+menuSelected, '*', FGColor, BGColor)
		} else {
			go setCellWG(&wg, menuSelectionX, ((height/2)-10)+3+i, ' ', FGColor, BGColor)
		}
	}
	wg.Wait()
	swg.Done()
}

func write(s string, startX, startY int, fgcolor, bgcolor termbox.Attribute, newStartX, clearRestOfLine bool, bgcolor2 termbox.Attribute) (int, int) {
	currentx := startX
	currenty := startY
	var wg1 sync.WaitGroup
	wg1.Add(len(s))
	for _, runeValue := range s {
		if runeValue == '\n' {
			/*if endings[currenty] == currentx {
				termbox.SetCell(currentx, currenty, ' ', fgcolor, termbox.ColorGreen)
			}*/
			go func(swg *sync.WaitGroup, currentx, currenty int, bgcolor2 termbox.Attribute) {
				var wg sync.WaitGroup
				for x := currentx; x < width; x++ {
					wg.Add(1)
					if x == editorLine+editorX-1 && currentState == "editor" {
						go setCellWG(&wg, x, currenty, '|', termbox.ColorCyan, bgcolor2)
					} else {
						go setCellWG(&wg, x, currenty, ' ', fgcolor, bgcolor2)
					}
				}
				wg.Wait()
				swg.Done()
			}(&wg1, currentx, currenty, bgcolor2)
			currenty++
			if newStartX {
				currentx = startX
			} else {
				currentx = editorX
			}
			continue
		} else if runeValue == '\r' {
			wg1.Done()
			continue // ???? Improve - May Be BUGGY - TODO
		} else if runeValue == '\t' {
			go writeWG(&wg1, tabString, currentx, currenty, fgcolor, bgcolor, false, false, bgcolor2)
			currentx += tabLength + 1
			continue
		}
		go setCellWG(&wg1, currentx, currenty, runeValue, fgcolor, bgcolor)
		currentx++
	}
	wg1.Wait()
	var wg2 sync.WaitGroup
	if clearRestOfLine {
		for x := currentx; x < width; x++ {
			wg2.Add(1)
			if x == editorLine+editorX-1 && currentState == "editor" {
				go setCellWG(&wg2, x, currenty, '|', termbox.ColorCyan, bgcolor2)
			} else {
				go setCellWG(&wg2, x, currenty, ' ', fgcolor, bgcolor2)
			}
		}
	}
	wg2.Wait()
	termbox.Flush()
	return currentx, currenty
}

func writeWG(swg *sync.WaitGroup, s string, startX, startY int, fgcolor, bgcolor termbox.Attribute, newStartX, clearRestOfLine bool, bgcolor2 termbox.Attribute) {
	write(s, startX, startY, fgcolor, bgcolor, newStartX, clearRestOfLine, bgcolor2)
	swg.Done()
}

func simpleWrite(s string, startX, startY int, fgcolor, bgcolor termbox.Attribute) (int, int) {
	currentx := startX
	currenty := startY
	var wg sync.WaitGroup
	wg.Add(len(s))
	for _, runeValue := range s {
		go setCellWG(&wg, currentx, currenty, runeValue, fgcolor, bgcolor)
		currentx++
	}
	wg.Wait()
	return currentx, currenty
}

func simpleWriteWG(swg *sync.WaitGroup, s string, startX, startY int, fgcolor, bgcolor termbox.Attribute) {
	simpleWrite(s, startX, startY, fgcolor, bgcolor)
	swg.Done()
}

func drawLine(startX, startY, endX, endY int, fgcolor, bgcolor termbox.Attribute, writeIndex string) (int, int) { // TODO: Improve to support diagonals
	var wg sync.WaitGroup
	for x := startX; x < endX; x++ {
		for y := startY; y <= endY; y++ {
			if termbox.CellBuffer()[x+y*width].Ch == ' ' {
				wg.Add(1)
				if writeIndex == "x" {
					go simpleWriteWG(&wg, strconv.Itoa(x), x, y, fgcolor, bgcolor)
				} else if writeIndex == "y" {
					if y >= 10 { // Improve for any addition of placevalue to work
						go simpleWriteWG(&wg, strconv.Itoa(y), x-1, y, fgcolor, bgcolor)
					} else {
						go simpleWriteWG(&wg, strconv.Itoa(y), x, y, fgcolor, bgcolor)
					}
				} else if writeIndex == "v" {
					go simpleWriteWG(&wg, "|", x, y, fgcolor, bgcolor)
				} else if writeIndex == "h" {
					go simpleWriteWG(&wg, "-", x, y, fgcolor, bgcolor)
				} else {
					go setCellWG(&wg, x, y, ' ', fgcolor, bgcolor)
				}
			}
		}
	}
	wg.Wait()
	termbox.Flush()
	return 0, endY + 1
}

func drawLineWG(swg *sync.WaitGroup, startX, startY, endX, endY int, fgcolor, bgcolor termbox.Attribute, writeIndex string) {
	drawLine(startX, startY, endX, endY, fgcolor, bgcolor, writeIndex)
	swg.Done()
}

func clearAll(doFlush bool) {
	//termbox.Clear(FGColor, BGColor)
	var wg sync.WaitGroup
	if currentState == "editor" {
		wg.Add(2)
		go drawEditorMenu(&wg)
		//termbox.SetCell(endx, endy, ' ', FGColor, termbox.ColorMagenta)
		go clearEditorPaneWG(&wg, false, true, false)
		ChangeCursor(editorX, editorY)
		//CurrentX, CurrentY = write(fmt.Sprintf("%s", Input), CurrentX, CurrentY, FGColor, BGColor, true)
	} else if currentState == "menu" {
		wg.Add(2)
		termbox.Clear(FGColor, BGColor)
		go drawMenu(&wg)
		go updateMenuSelected(&wg)
		//termbox.HideCursor()
	}
	wg.Wait()
	if doFlush {
		termbox.Flush()
	}
}

func clearEditorPane(doFlush, doUpdate, doChangeCursor bool) {
	if currentState == "editor" {
		var wg sync.WaitGroup
		for i := 0; i < width*height; i++ {
			wg.Add(1)
			var x = int(i % width)
			var y = int(i / width)
			if x >= 0 && x <= width && y >= 0 && y <= editorY-1 {
				wg.Done()
				continue
			} else if x >= 0 && x <= editorX-1 && y >= 0 && y <= height {
				wg.Done()
				continue
			} else if y >= height-editorBY && y <= height-1 && x >= 0 && x <= width {
				wg.Done()
				continue
			} else {
				go setCellWG(&wg, x, y, ' ', FGColor, BGColor)
			}
		}
		//termbox.SetCell(endx, endy, ' ', FGColor, termbox.ColorMagenta)
		wg.Wait()
		if doUpdate {
			if doChangeCursor {
				CurrentX, CurrentY = write(fmt.Sprintf("%s", Input), editorX, editorY, FGColor, BGColor, true, false, BGColor)
			} else {
				write(fmt.Sprintf("%s", Input), editorX, editorY, FGColor, BGColor, true, false, BGColor)
			}
			termbox.SetCursor(CurrentX, CurrentY)
		}
		var wg2 sync.WaitGroup
		wg2.Add(1)
		go drawEditorLine(&wg2)
		wg2.Wait()
		if doFlush {
			termbox.Flush()
		}
	}
}

func clearEditorPaneWG(swg *sync.WaitGroup, doFlush, doUpdate, doChangeCursor bool) {
	clearEditorPane(doFlush, doUpdate, doChangeCursor)
	swg.Done()
}

func updateEditorPane(currentx, currenty int, newLine, deleteLine bool) (int, int) {
	x := editorX
	y := editorY
	if newLine || deleteLine { // Make more efficient by clearing the lines up to the ending of the currenty + 1 - TODO
		var wg sync.WaitGroup
		wg.Add(1)
		var textLocation = getTextLocation(currentx, currenty, 0, width, editorY, 0, endings)
		go writeWG(&wg, Input[textLocation:]+tabString, currentx, currenty, FGColor, BGColor, false, true, BGColor)
		if deleteLine {
			for x := editorX; x < width; x++ {
				wg.Add(1)
				if x == editorLine+editorX-1 {
					go setCellWG(&wg, x, endy+1, '|', termbox.ColorCyan, BGColor)
				} else {
					go setCellWG(&wg, x, endy+1, ' ', FGColor, BGColor)
				}
			}
		}
		wg.Wait()
	} else {
		var textLocationStart = getTextLocation(currentx, currenty, 0, width, editorY, 0, endings)
		var textLocationEnd = getLineEnd(textLocationStart)
		x, y = write(Input[textLocationStart:textLocationEnd]+tabString, currentx, currenty, FGColor, BGColor, false, false, BGColor)
	}
	return x, y
}

func updateMenus(doFlush bool) {
	var wg sync.WaitGroup
	if currentState == "editor" {
		wg.Add(2)
		go drawEditorMenu(&wg)
		go drawEditorLine(&wg)
		ChangeCursor(editorX, editorY)
	} else if currentState == "menu" {
		wg.Add(2)
		go drawMenu(&wg)
		go updateMenuSelected(&wg)
	}
	termbox.HideCursor()
	wg.Wait()
	if doFlush {
		termbox.Flush()
	}
}

func clear(menus, editor, doFlush, doUpdate, doChangeCursor bool) {
	if menus && editor {
		clearAll(doFlush)
	} else if menus {
		updateMenus(doFlush)
	} else if editor {
		clearEditorPane(doFlush, doUpdate, doChangeCursor)
	}
}

func clearAfter(x, y int) {
	var wg sync.WaitGroup
	if x == 0 {
		x = CurrentX
		for y2 := y; y2 < height-editorBY; y2++ {
			wg.Add(1)
			go setCellWG(&wg, x, y2, ' ', FGColor, BGColor)
		}
	} else if y == 0 {
		y = CurrentY
		for x2 := x; x2 < width; x2++ {
			wg.Add(1)
			go setCellWG(&wg, x2, y, ' ', FGColor, BGColor)
		}
	} else {
		for y2 := y; y2 < height-editorBY; y2++ {
			for x2 := x; x2 < width; x2++ {
				wg.Add(1)
				go setCellWG(&wg, x2, y2, ' ', FGColor, BGColor)
			}
		}
	}
	wg.Wait()
}

func setCellWG(swg *sync.WaitGroup, x, y int, ch rune, fg, bg termbox.Attribute) {
	termbox.SetCell(x, y, ch, fg, bg)
	swg.Done()
}

