package main

import (
	"fmt"

	"github.com/nsf/termbox-go"
)

var editorLine = 80
var leaveX = 0
var leaveY = 0
var tabString = "    "
var tabLength = len(tabString)
var tabMode = "spaces" // The tabMode can be "spaces" or "tabChars"
var highestX = editorX
var status = "Opened New File"

// -----[
/*
var CurrentY = 0
var CurrentX = 0
var editorX = 6
var editorY = 1
var editorRX = 0 // how many cells to leave to the right side
var editorBY = 1 // how many cells to leave to the bottom
var endx = editorX
var endy = editorY

// What is currently written out by the user
var Input string
var exit = false

var endings [1000]int      // The ending of each line in xpositions
var indentLevels [1000]int // Level of indentation on each line, 0 for no indentation
*/
// ]-----

func editorState(input <-chan termbox.Event, msgInput chan string) {
	select {
	case in := <-input:
		//isParsing = true
		ParseInputEditor(in)
		isParsing = false
		// TODO
	case mIn := <-msgInput:
		//isParsing = true
		ParseMessage(mIn)
		isParsing = false
		// TODO
	default:
		// Do nothing
	}
}


func ParseInputEditor(e termbox.Event) {
	switch e.Type {
	case termbox.EventMouse:
		mx := e.MouseX
		my := e.MouseY

		if mx >= editorX && my >= editorY && my <= height-1-editorBY {
			CurrentY = my
			if endings[CurrentY] != 0 {
				if mx > endings[CurrentY] {
					CurrentX = endings[CurrentY]
				} else {
					CurrentX = mx
				}
			} else if my == endy {
				if mx > endx {
					CurrentX = endx
				} else {
					CurrentX = mx
				}
				CurrentY = endy
			} else {
				if mx > endx {
					CurrentX = endx
				} else {
					CurrentX = mx
				}
				CurrentY = endy
			}
		}
		termbox.SetCursor(CurrentX, CurrentY)
		if currentState == "editor" {
			updateEditorStatus(0)
		}
		termbox.Flush()
	case termbox.EventResize: // Make it clear only when done resizing just after you resized! TODO
		oldx := CurrentX
		oldy := CurrentY
		width = e.Width
		height = e.Height - 1 // Must subtract 1 from this for Windows OS only!
		clearAll(false)
		ChangeCursor(oldx, oldy)
		termbox.SetCursor(CurrentX, CurrentY)
		termbox.Flush()
	case termbox.EventKey:
		switch e.Key {
		case termbox.KeyArrowLeft:
			var sub int
			var indent string
			lessThan := indentLevels[CurrentY] + 1
			for i := 1; i < lessThan; i++ {
				indent = fmt.Sprint(indent, tabString)
			}
			indentLength := len(indent)
			if indentLevels[CurrentY] == 0 || CurrentX == editorX {
				sub = 1
			} else if CurrentX == indentLength+editorX {
				sub = 4
			} else if CurrentX < indentLength+editorX && indentLength > 4 { // TODO: Make more efficient by making variable for indentLength + editorX
				for i := indentLength + editorX - 4; i > editorX+3; i -= 4 {
					if CurrentX == i {
						sub = 4
						break
					}
				}
			} else {
				sub = 1
			}

			if CurrentX > editorX {
				CurrentX -= sub
			} else if CurrentX == editorX && CurrentY > editorY {
				CurrentY--
				CurrentX = endings[CurrentY]
			}
		case termbox.KeyArrowRight:
			var add int
			var indent string
			for i := 1; i < indentLevels[CurrentY]+1; i++ {
				indent = fmt.Sprint(indent, tabString)
			}
			indentLength := len(indent)
			if indentLevels[CurrentY] == 0 {
				add = 1
			} else if CurrentX == indentLength+editorX-4 {
				add = 4
			} else if CurrentX < indentLength+editorX-4 {
				for i := indentLength + editorX - 4; i >= editorX; i -= 4 {
					if CurrentX == i-4 {
						add = 4
						break
					}
				}
			} else {
				add = 1
			}

			if termbox.CellBuffer()[CurrentX+CurrentY*width].Ch == ' ' {
				var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
				if textLocation < len(Input) {
					if Input[textLocation] == ' ' {
						CurrentX += add
					} else if endings[CurrentY] == CurrentX {
						CurrentY++
						CurrentX = editorX
					}
				}
			} else {
				CurrentX += add
			}
		case termbox.KeyArrowUp:
			highestX = CurrentX // TODO - Redo highestX
			var indent string
			for i := 1; i < indentLevels[CurrentY-1]+1; i++ {
				indent = fmt.Sprint(indent, tabString)
			}
			indentLength := len(indent)
			if CurrentY > editorY {
				if highestX > endings[CurrentY-1] {
					CurrentY--
					CurrentX = endings[CurrentY]
				} else {
					if highestX > indentLength+editorX-1 {
						CurrentY--
						CurrentX = highestX
					} else {
						for i := indentLength + editorX - 4; i >= editorX; i -= 4 {
							if highestX >= i && highestX <= i+4 {
								//add = 4
								CurrentX = i
								CurrentY--
								break
							}
						}
					}
				}
			}
		case termbox.KeyArrowDown:
			highestX = CurrentX
			var indent string
			for i := 1; i < indentLevels[CurrentY+1]+1; i++ {
				indent = fmt.Sprint(indent, tabString)
			}
			indentLength := len(indent)
			if endings[CurrentY+1] != 0 {
				if highestX > endings[CurrentY+1] {
					CurrentY++
					CurrentX = endings[CurrentY]
				} else {
					if highestX > indentLength+editorX-1 {
						CurrentY++
						CurrentX = highestX
					} else {
						for i := indentLength + editorX - 4; i >= editorX; i -= 4 {
							if highestX >= i && highestX <= i+4 {
								//add = 4
								CurrentX = i
								CurrentY++
								break
							}
						}
					}
				}
			} else if CurrentY+1 == endy {
				if highestX > endx {
					CurrentY++
					CurrentX = endx
				} else {
					if highestX > indentLength+editorX-1 {
						CurrentY++
						CurrentX = highestX
					} else {
						for i := indentLength + editorX - 4; i >= editorX; i -= 4 {
							if highestX >= i && highestX <= i+4 {
								//add = 4
								CurrentX = i
								CurrentY++
								break
							}
						}
					}
				}
			}
		case termbox.KeyBackspace:
			if !(CurrentX == editorX && CurrentY == editorY) {
				oldx := CurrentX
				oldy := CurrentY

				var sub int
				var indent string
				for i := 1; i < indentLevels[oldy]+1; i++ {
					indent = fmt.Sprint(indent, tabString)
				}
				indentLength := len(indent)
				if indentLevels[oldy] == 0 || CurrentX == editorX {
					sub = 1
				} else if oldx == indentLength+editorX {
					sub = 4
					indentLevels[oldy]--
				} else if oldx < indentLength+editorX && indentLength > 4 {
					for i := indentLength + editorX - 4; i > editorX+3; i -= 4 {
						if CurrentX == i {
							sub = 4
							indentLevels[oldy]--
							break
						}
					}
				} else {
					sub = 1
				}

				if CurrentX > editorX {
					// Move cursor back one character
					CurrentX -= sub
					// Else if Cursor at beginning of line
				} else if CurrentX == editorX && CurrentY > editorY {
					// Move cursor to end of previous line, where the return character is
					CurrentX = endings[CurrentY-1]
					CurrentY--
				}

				var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)

				Input = fmt.Sprint(Input[0:textLocation], Input[textLocation+sub:len(Input)])

				if oldx > editorX {
					if oldy == endy {
						endx -= sub
						endings[CurrentY] = 0
					} else {
						endings[CurrentY] -= sub
					}
					updateEditorPane(CurrentX, CurrentY, false, false) // BUGGY - TODO
				} else if oldx == editorX {
					if oldy == endy {
						endy--
						endings[oldy] = 0
						endx = endings[CurrentY] + (endx - editorX)
					} else {
						endy--
						endings[CurrentY] = endings[oldy] + (endings[CurrentY] - editorX)
						for i := oldy + 1; i < len(endings)-1; i++ {
							endings[i-1] = endings[i]
							indentLevels[i-1] = indentLevels[i]
						}
					}
					updateEditorPane(CurrentX, CurrentY, false, true)
				}

				//newx := CurrentX
				//newy := CurrentY
				//clearEditorPane(false, true, false)
				//ChangeCursor(newx, newy)
				termbox.SetCursor(CurrentX, CurrentY)
			}
		case termbox.KeyBackspace2: // BUG FOUND when Backspacing a line with an enter at beginning - the endy does not move properly
			if !(CurrentX == editorX && CurrentY == editorY) {
				oldx := CurrentX
				oldy := CurrentY

				var sub int
				var indent string
				for i := 1; i < indentLevels[oldy]+1; i++ {
					indent = fmt.Sprint(indent, tabString)
				}
				indentLength := len(indent)
				if indentLevels[oldy] == 0 || CurrentX == editorX {
					sub = 1
				} else if oldx == indentLength+editorX {
					sub = 4
					indentLevels[oldy]--
				} else if oldx < indentLength+editorX && indentLength > 4 {
					for i := indentLength + editorX - 4; i > editorX+3; i -= 4 {
						if CurrentX == i {
							sub = 4
							indentLevels[oldy]--
							break
						}
					}
				} else {
					sub = 1
				}

				if CurrentX > editorX {
					// Move cursor back one character
					CurrentX -= sub
					// Else if Cursor at beginning of line
				} else if CurrentX == editorX && CurrentY > editorY {
					// Move cursor to end of previous line, where the return character is
					CurrentX = endings[CurrentY-1]
					CurrentY--
				}

				var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)

				Input = fmt.Sprint(Input[0:textLocation], Input[textLocation+sub:len(Input)])

				if oldx > editorX {
					if oldy == endy {
						endx -= sub
						endings[CurrentY] = 0
					} else {
						endings[CurrentY] -= sub
					}
					updateEditorPane(CurrentX, CurrentY, false, false) // BUGGY - TODO
				} else if oldx == editorX {
					if oldy == endy {
						endy--
						endings[oldy] = 0
						endx = endings[CurrentY] + (endx - editorX)
					} else {
						endy--
						endings[CurrentY] = endings[oldy] + (endings[CurrentY] - editorX)
						for i := oldy + 1; i < len(endings)-1; i++ {
							endings[i-1] = endings[i]
							indentLevels[i-1] = indentLevels[i]
						}
					}
					updateEditorPane(CurrentX, CurrentY, false, true)
				}

				//newx := CurrentX
				//newy := CurrentY
				//clearEditorPane(false, true, false)
				//ChangeCursor(newx, newy)
				termbox.SetCursor(CurrentX, CurrentY)
			}
		case termbox.KeyDelete:
			if !(CurrentX == endx && CurrentY == endy) {
				oldx := CurrentX
				oldy := CurrentY
				var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)

				if !(CurrentX == endx && CurrentY == endy) {
					Input = fmt.Sprint(Input[0:textLocation], Input[textLocation+1:len(Input)])
					if CurrentX == endings[CurrentY] {
						if CurrentY == endy-1 { // TODO - Check
							indentLevels[CurrentY] = indentLevels[endy]
							endings[endy] = 0
							indentLevels[endy] = 0
							endy--
							//endings[endy] = 0
							endx = endings[CurrentY] + (endx - editorX)
							endings[CurrentY] = 0
						} else {
							endings[CurrentY] = endings[CurrentY] + (endings[CurrentY+1] - editorX)
							endings[endy] = 0
							indentLevels[endy] = 0
							endy--
							//endings[endy] = 0
							lessThan := len(endings) - 1
							for i := CurrentY + 2; i < lessThan; i++ {
								endings[i-1] = endings[i]
								indentLevels[i-1] = indentLevels[i]
							}
						}
						updateEditorPane(oldx, oldy, false, true)
					} else if CurrentY == endy {
						endx--
						updateEditorPane(oldx, oldy, false, false)
					} else {
						endings[CurrentY]--
						updateEditorPane(oldx, oldy, false, false)
					}
					//clearEditorPane(false, true, false)
					ChangeCursor(oldx, oldy)
					termbox.SetCursor(CurrentX, CurrentY)
				}
			}
		case termbox.KeySpace:
			var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
			Input = addKeyToInput(textLocation, Input, ' ', true)

			updateEditorPane(CurrentX, CurrentY, false, false)
			CurrentX++
		case termbox.KeyTab: // TODO: check tabMode
			var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
			var currentIndent string
			for i := 0; i < indentLevels[CurrentY]; i++ {
				currentIndent = fmt.Sprint(currentIndent, tabString)
			}
			currentIndentLength := len(currentIndent)
			if CurrentX == currentIndentLength+editorX {
				indentLevels[CurrentY]++
			} else if CurrentX < currentIndentLength+editorX {
				for i := currentIndentLength + editorX - 4; i >= editorX; i -= 4 {
					if CurrentX == i {
						indentLevels[CurrentY]++
						break
					}
				}
			}

			Input = addTextToInput(textLocation, Input, tabString, true)

			updateEditorPane(CurrentX, CurrentY, false, false)
			CurrentX += tabLength
		case termbox.KeyEnter:

			newx := 0 // Where the cursor will be at the end of all operations
			newy := 0
			newendx := 0 // Where the endx and endy will be at the end of all operations
			newendy := 0

			var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
			Input = addKeyToInput(textLocation, Input, '\n', false)

			if CurrentY == endy && CurrentX == endx {
				newendy = CurrentY + 1
				newendx = editorX
				newx = editorX
				newy = CurrentY + 1
				endings[newy] = 0
				indentLevels[newy] = 0
			} else if CurrentY == endy && CurrentX != endx {
				newendy = CurrentY + 1
				newendx = (endx - CurrentX) + editorX
				newx = editorX
				newy = CurrentY + 1
				endings[newy] = 0
				indentLevels[newy] = 0
			} else if CurrentY != endy && CurrentX != endx {
				if CurrentX == endings[CurrentY] { // BUG when entering on line where ending is at editorX and endy is next line and endx is at editorX, the cursor goes to 0, 0 - TODO
					newendy = endy + 1
					newendx = endx
					newx = editorX
					newy = CurrentY + 1
					greaterThan := newy - 1
					for i := len(endings) - 2; i > greaterThan; i-- {
						endings[i+1] = endings[i]
						indentLevels[i+1] = indentLevels[i]
					}
					endings[newy] = editorX
					indentLevels[newy] = 0
				} else {
					newendy = endy + 1
					newendx = endx
					newx = editorX
					newy = CurrentY + 1
					greaterThan := newy - 1
					for i := len(endings) - 2; i > greaterThan; i-- {
						endings[i+1] = endings[i]
						indentLevels[i+1] = indentLevels[i]
					}
					endings[newy] = (endings[CurrentY] - CurrentX) + editorX
					indentLevels[newy] = 0 // TODO - parse how many spaces before first nonspace character, divide by 4 (discard decimals) to get indentation level
				}
			}

			endx = newendx
			endy = newendy
			endings[CurrentY] = CurrentX

			if textLocation > 1 && Input[textLocation-1] == '{' {
				ChangeCursor(newx, newy)
				var newTextLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
				var indent string
				for i := 0; i < indentLevels[newy-1]+1; i++ {
					indent = fmt.Sprint(indent, tabString)
				}
				Input = addTextToInput(newTextLocation, Input, indent, true)
				clearAfter(CurrentX, 0)
				updateEditorPane(CurrentX, CurrentY, true, false)
				indentLevels[newy] = 1 + indentLevels[newy-1]
				ChangeCursor(newx+len(indent), newy)
			} else {
				clearAfter(CurrentX, 0)
				ChangeCursor(newx, newy)
				if indentLevels[newy-1] > 0 {
					var newTextLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
					var indent string
					for i := 0; i < indentLevels[newy-1]; i++ {
						indent = fmt.Sprint(indent, tabString)
					}
					Input = addTextToInput(newTextLocation, Input, indent, true)
					indentLevels[newy] += int(len(indent) / tabLength)
					updateEditorPane(CurrentX, CurrentY, true, false)
					ChangeCursor(newx+len(indent), newy)
				} else {
					updateEditorPane(CurrentX, CurrentY, true, false)
				}
			}
		case termbox.KeyCtrlV: // Not Working - TODO
			var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
			Input = addTextToInput(textLocation, Input, Input, false)

			CurrentX, CurrentY = updateEditorPane(CurrentX, CurrentY, false, false) // TODO - Add option to place cursor after Input! - DONE???
		case termbox.KeyCtrlN:
			Input, endings = clearInput(Input, endings)
			clearEditorPane(false, false, false)
			ChangeCursor(editorX, editorY)
		case termbox.KeyEsc:
			currentState = "menu"
			currentMenu = menuOpenOptions
			menuSelected = 0
			leaveX = CurrentX
			leaveY = CurrentY
			clearAll(false)
		case termbox.KeyCtrlE:
			CurrentX = endx
			CurrentY = endy
		case termbox.KeyCtrlB:
			CurrentX = editorX
			CurrentY = editorY
		case termbox.KeyEnd:
			if CurrentY == endy {
				CurrentX = endx
			} else {
				CurrentX = endings[CurrentY]
			}
		case termbox.KeyHome:
			CurrentX = editorX
		default:
			var textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)

			if e.Ch == '{' {
				Input = addTextToInput(textLocation, Input, "{}", true)
			} else if e.Ch == '(' {
				Input = addTextToInput(textLocation, Input, "()", true)
			} else if e.Ch == '[' {
				Input = addTextToInput(textLocation, Input, "[]", true)
			} else if e.Ch == '<' {
				Input = addTextToInput(textLocation, Input, "<>", true)
			} else if e.Ch == '}' && textLocation != len(Input) {
				if Input[textLocation] != '}' {
					Input = addKeyToInput(textLocation, Input, e.Ch, true)
				}
			} else if e.Ch == ')' && textLocation != len(Input) {
				if Input[textLocation] != ')' {
					Input = addKeyToInput(textLocation, Input, e.Ch, true)
				}
			} else if e.Ch == ']' && textLocation != len(Input) {
				if Input[textLocation] != ']' {
					Input = addKeyToInput(textLocation, Input, e.Ch, true)
				}
			} else if e.Ch == '>' && textLocation != len(Input) {
				if Input[textLocation] != '>' {
					Input = addKeyToInput(textLocation, Input, e.Ch, true)
				}
			} else if e.Ch == ';' && textLocation != len(Input) {
				if Input[textLocation] == ')' {
					CurrentX++
					textLocation = getTextLocation(CurrentX, CurrentY, 0, width, editorY, 0, endings)
					Input = addKeyToInput(textLocation, Input, e.Ch, true)
				} else {
					Input = addKeyToInput(textLocation, Input, e.Ch, true)
				}
			} else {
				Input = addKeyToInput(textLocation, Input, e.Ch, true)
			}

			//oldx := CurrentX + 1
			//oldy := CurrentY
			updateEditorPane(CurrentX, CurrentY, false, false)
			CurrentX++
			//CurrentY = oldy
		}
		termbox.SetCursor(CurrentX, CurrentY)
		if currentState == "editor" {
			updateEditorStatus(0)
		}
		termbox.Flush()
	}
}

func addKeyToInput(textLocation int, input string, c rune, changeCursor bool) string {
	if changeCursor && c != '\n' {
		if textLocation == len(input) {
			input = fmt.Sprint(input, string(c))
			endy = CurrentY
			endx = CurrentX + 1
		} else {
			input = fmt.Sprint(input[0:textLocation], string(c), input[textLocation:len(input)])
			if CurrentY == endy {
				endx++
			} else {
				endings[CurrentY]++
			}
		}
	} else {
		input = fmt.Sprint(input[0:textLocation], string(c), input[textLocation:len(input)])
	}
	return input
}

func addTextToInput(textLocation int, input, text string, changeCursor bool) string {
	if textLocation == len(input) {
		input = fmt.Sprint(input, text)
		endy = CurrentY
		endx = CurrentX + len(text)
	} else {
		input = fmt.Sprint(input[0:textLocation], text, input[textLocation:len(input)])
		if CurrentY == endy {
			endx += len(text)
		} else {
			endings[CurrentY] += len(text)
		}
	}
	return input
}

func getTextLocation(x, y, location, width, editorY, yom int, endings [1000]int) int {
	var textLocation int

	if location == 0 {
		location = getLocation(x, y, width)
	}

	if y == 1 {
		textLocation = location - (editorX * y) - width - yom
	} else if y > 1 {
		textLocation = location - (editorX * y) - width + (y - 1)
		for i := editorY; i < y; i++ {
			textLocation = textLocation - (width - endings[i])
		}
	}

	return textLocation
}

func clearInput(input string, endings [1000]int) (string, [1000]int) {
	for i := 0; i < len(endings); i++ {
		endings[i] = 0
		indentLevels[i] = 0
	}
	endx = editorX
	endy = editorY
	return "", endings
}

func getLineEnd(textLocationStart int) int {
	for i := textLocationStart; i < len(Input); i++ {
		if Input[i] == '\n' {
			return i
		}
	}
	return len(Input)
}

