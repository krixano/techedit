# TechEdit #

A very simple text editor written in Go(lang) using the Termbox-go library.

**See the much improved Rust Version on my Github: https://github.com/krixano/**