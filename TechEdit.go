package main

import (
	//"bitbucket.org/musicchristian/techcore"
	"github.com/nsf/termbox-go"
)

var BGColor = termbox.ColorBlue
var FGColor = termbox.ColorWhite

var width int
var height int

var currentState = "menu" // Can be menu or editor

func main() {
	err := termbox.Init()
	defer termbox.Close()

	if err != nil {
		panic(err)
	}

	//termbox.SetOutputMode(termbox.Output256) // This does not support windows!
	termbox.SetInputMode(termbox.InputEsc | termbox.InputMouse)

	width, height = termbox.Size()

	input := make(chan termbox.Event, 50)
	msgInput := make(chan string)

	go inputManager(input)
	go timeHandler(msgInput)
	programManager(input, msgInput)
}
