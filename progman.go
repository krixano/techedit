package main

import (
		"strings"
		"github.com/nsf/termbox-go"
)

var CurrentY = 0
var CurrentX = 0
var editorX = 6
var editorY = 1
var editorRX = 0 // how many cells to leave to the right side
var editorBY = 1 // how many cells to leave to the bottom
var endx =  editorX
var endy = editorY

// What is currently written out by the user
var Input string
var exit = false

var endings [1000]int // The ending of each line in xpositions
var indentLevels [1000]int // Level of indentation on each line, 0 for no indentation

func programManager(input <-chan termbox.Event, msgInput chan string) {
		clearAll(true)
		for {
				if currentState == "menu" {
						menuState(input, msgInput)
				} else if currentState == "editor" {
						editorState(input, msgInput)
				}
				if exit {
						break;
				}
		}
}

func ParseMessage(mIn string) {
		if strings.HasPrefix(mIn, "progman:") {
				switch mIn {
				case "progman:updateTime":
						if currentState == "editor" {
								write(CurrentTime, width-5, 0, termbox.ColorBlack, termbox.ColorCyan, false, true, termbox.ColorCyan)
						} else {
								write(CurrentTime, width-5, 0, termbox.ColorBlack, termbox.ColorCyan, false, true, BGColor)
						}
						termbox.Flush()
				}
		}
}

func ChangeCursor(x, y int) {
		CurrentX = x
		CurrentY = y
}

func getLocation(x, y, width int) int {
		return x + (y * width)
}
