package main

import (
	"github.com/nsf/termbox-go"
	"sync"
)

var menuDefaultOptions = []string{
	"New File",
	"Open File",
	"Exit",
}

var menuOpenOptions = []string{
	"Back",
	"New File",
	"Open File",
	"Save File",
	"Exit",
}

var isParsing = false
var menuSelected int
var currentMenu = menuDefaultOptions

func menuState(input <-chan termbox.Event, msgInput chan string) {
	//termbox.SetCursor(CurrentX, CurrentY)
	termbox.HideCursor()
	//clearAll(true)
	select {
	case in := <-input:
		ParseInputMenu(in)
		// TODO
	case mIn := <-msgInput:
		ParseMessage(mIn)
		// TODO
	}
}

func ParseInputMenu(e termbox.Event) {
	switch e.Type {
	case termbox.EventResize:
		width = e.Width
		height = e.Height
		menuSelectionX = (width / 2) - (len(menuTitle) / 4) + 1
		clearAll(false)
	case termbox.EventKey:
		switch e.Key {
		case termbox.KeyArrowDown:
			if menuSelected != len(currentMenu)-1 {
				menuSelected++
			} else {
				menuSelected = 0
			}
			var wg sync.WaitGroup
			wg.Add(1)
			//clearAll(false)
			go updateMenuSelected(&wg)
			wg.Wait()
		case termbox.KeyArrowUp:
			if menuSelected != 0 {
				menuSelected--
			} else {
				menuSelected = len(currentMenu) - 1
			}
			var wg sync.WaitGroup
			wg.Add(1)
			//clearAll(false)
			go updateMenuSelected(&wg)
			wg.Wait()
		case termbox.KeyEnter:
			switch currentMenu[menuSelected] {
			case "New File":
				currentState = "editor"
				status = "Opened New File"
				Input, endings = clearInput(Input, endings)
				clearAll(false)
				termbox.SetCursor(editorX, editorY)
			case "Exit":
				exit = true
			case "Back":
				currentState = "editor"
				clearAll(false)
				CurrentX = leaveX
				CurrentY = leaveY
				termbox.SetCursor(CurrentX, CurrentY)
			case "Save File":
			case "Open File":
			}
		case termbox.KeyCtrlZ:
			exit = true
		}
		//termbox.SetCursor(CurrentX, CurrentY)
	}
	termbox.Flush()
}

