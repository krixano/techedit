package main

import (
	"fmt"
	"time"
)

var CurrentTime string

func timeHandler(msgInput chan<- string) {
	var oldh, oldm int
	var h, m int
	for {
		if exit {
			break
		}
		oldh = h
		oldm = m
		h, m, _ = time.Now().Clock()
		if oldh != h || oldm != m {
			CurrentTime = fmt.Sprintf("%d:%d", h, m)
			msgInput <- "progman:updateTime"
		}
	}
}
