package main

import "github.com/nsf/termbox-go"

func inputManager(input chan<- termbox.Event) {
	for {
		if !isParsing {
			input <- termbox.PollEvent()
		}
		if exit {
			close(input)
			break
		}
	}
}

